# solidService.js

A set of functions to help you get started with authentication and reading/writing basic web tracking data from a Solid data pod. Solid is a specification that lets people store their data securely in decentralized data stores called Pods. [Learn more about the Solid Project](https://solidproject.org). 

## Dependencies

solidService uses [Inrupt JavaScript Client libraries](https://docs.inrupt.com/developer-tools/javascript/client-libraries).

## How to use

Clone the repository, adapt the solidService.js module to your needs and use it inside your project.

## Authentication

Call the `solid-client-authn-browser` library’s login() function to start the process. Pass in the following login options:

- `oidcIssuer` Set to the user’s Solid Identity Provider (where the login function will send the user).
- `redirectUrl` Set to the location that the Solid Identity Provider will send the user back once logged in.
- `clientName` (Optional) Set to the display name for the client during the login process. 
    When logging in, the user has to approve the client’s access to the requested data. The clientName is the name displayed during the approval step. If clientName is not provided, a random identifier is generated and used for the name.

```
import { handleIncomingRedirect, login, fetch, getDefaultSession } from '@inrupt/solid-client-authn-browser'

async function loginAndFetch() {
    await handleIncomingRedirect();

    // Start the Login Process if not already logged in.
    if (!getDefaultSession().info.isLoggedIn) {
        await login({
            // Specify the URL of the user's Solid Identity Provider; e.g., "https://broker.pod.inrupt.com" or "https://inrupt.net"
            oidcIssuer: "https://broker.pod.inrupt.com",
            // Specify the URL the Solid Identity Provider should redirect to after the user logs in,
            // e.g., the current page for a single-page app.
            redirectUrl: window.location.href,
            // Pick an application name that will be shown when asked 
            // to approve the application's access to the requested data.
            clientName: "My application"
        })
    }
}

loginAndFetch();
```

For more information, visit the [the Inrupt docs](https://docs.inrupt.com/developer-tools/javascript/client-libraries/tutorial/authenticate-browser/).

**Session Restore upon Browser Refresh**

For security reasons, solid-client-authn-browser does not store access tokens in the localStorage or any other place that persists across page refresh.

This means that upon page refresh/reload, the access token is lost, and your web application will be logged out. Use `solid-client-authn-browser`’s `handleIncomingRedirect()` function to log your application back in without user interaction. To automatically log the user back in after a page refresh, in your application, call `handleIncomingRedirect()` with restorePreviousSession set true.

To route the user back to the refreshed page instead of to the redirectURL, you can make use of the sessionRestore event and the associated callback onSessionRestore() function. With session restore enabled and the user returns back to the application after the silent authentication, the sessionRestore event is fired.

```
import {
    handleIncomingRedirect, 
    onSessionRestore
} from "@inrupt/solid-client-authn-browser";
import { useEffect } from 'react';
import { useRouter } from 'next/router'

export default function MyApp() {

    const router = useRouter();

    // 1. Register the callback to restore the user's page after refresh and
    //    redirection from the Solid Identity Provider.
    onSessionRestore((url) => {
        router.push(url)
    });

    useEffect(() => {
        // 2. When loading the component, call `handleIncomingRedirect` to authenticate
        //    the user if appropriate, or to restore a previous session.
        handleIncomingRedirect({
            restorePreviousSession: true
        }).then((info) => {
            console.log(`Logged in with WebID [${info.webId}]`)
        })
    }, []);

    // ... rest of the component, where `login()` should be called to initiate the
    // login process.
}
```

To read more about session restore upon browser refresh, check out [the Inrupt docs](https://docs.inrupt.com/developer-tools/javascript/client-libraries/tutorial/restore-session-browser-refresh/).

## Methods

The main methods to use in your project are:

**isLoggedIn()**

Verify if current user is authenticated with a Solid Pod.

```
var isLoggedIn = solidService.isLoggedIn();
```

**writeWebTrackingDataToPod(data)**

Writes a JSON data object to an authenticated Solid pod in [Turtle data format](https://www.w3.org/TR/turtle). In the current implementation, the data object must have:

* `event` The event name, e.g. "page-impression"
* `category` The category the page belongs to
* `url` The url of the page

```
const data = {
    event: "page-impression",
    category: "Sports",
    url: "https://www.example.com"
}
solidService.writeWebTrackingDataToPod(data);
```

**readWebTrackingDataFromPod()**

Read web tracking data from the Solid pod, and return in JSON format.

```
const result = await solidService
    .readWebTrackingDataFromPod()
    .then((response) => {
        return response
    })
```

##### version 1.0.0
* Intitial version

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details