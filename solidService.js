import { getDefaultSession, fetch } from "@inrupt/solid-client-authn-browser";
import {
    buildThing,
    createAcl,
    createSolidDataset,
    createThing,
    getDatetime,
    getSolidDataset,
    getStringNoLocale,
    getThingAll,
    getUrl,
    saveAclFor,
    saveSolidDatasetAt,
    setAgentResourceAccess,
    setThing,
} from "@inrupt/solid-client";
import { RDF } from "@inrupt/vocab-common-rdf";

const THING_TYPE_PROPERTY = "http://example#gddl";
const SCHEMA_NAME_PROPERTY = "https://schema.org/name";
const SCHEMA_CATEGORY_PROPERTY = "https://schema.org/category";
const SCHEMA_URL_PROPERTY = "https://schema.org/url";
const SCHEMA_DATE_PROPERTY = "https://schema.org/date";

const solidService = {
    /**
     * Checks if the user is logged in.
     * 1) Get the current session.
     * 2) Check whether the user is logged in.
     *
     * @returns whether the user is logged in.
     */
    isLoggedIn: () => {
        // 1)
        const session = getDefaultSession();
        // 2)
        return session?.info?.isLoggedIn;
    },

    /**
     * Write webtracking data to the pod.
     * 1) Extract the nescessary elements from the data
     * 2) Write the data to the pod.
     *
     * @param {*} data  webtracking data to send to the pod
     */
    writeWebTrackingDataToPod: (data) => {
        // 1)
        const { eventName, category, hostUrl } = extractData(data);
        // 2)
        writeDataToPod(eventName, category, hostUrl);
    },

    /**
     * Read webtracking data from the pod.
     *
     * @returns webtracking data in JSON format in a Promise, or throws an error when user is not authenticated with their pod.
     */
    readWebTrackingDataFromPod: async () => {
        if (!getDefaultSession().info.isLoggedIn) {
            throw new Error("Not authenticated.");
        }
        return readTrackingData();
    },
};

/**********************************/
/*  START - READING DATA FROM POD */
/**********************************/
/**
 * Fetch a dataset on a certain url.
 *
 * @param {*} resourceUrl   resource url of the tracking data in the users' pod
 * @returns the fetched dataset in a Promise (or throws an error).
 */
const fetchSolidDataset = async (resourceUrl) => {
    return getSolidDataset(resourceUrl, { fetch: fetch });
};

/**
 * Read the webtracking data from the pod.
 * 1) Build the resource url.
 * 2) Fetch the dataset (resolves in a solid-dataset, throws error (404) when dataset was not found at "resourceUrl").
 * 3) Get all things (nodes) from the turtle dataset.
 * 4) Parse the nodes to a list of JSON objects.
 *
 * @returns JSON list of webtracking data.
 */
const readTrackingData = async () => {

    try {
        // 1)
        const resourceUrl = buildResourceUrl();
        // 2)
        const dataset = await fetchSolidDataset(resourceUrl);
        // 3)
        const allThings = getThingAll(dataset);
        // 4)
        return turtleWebtrackingListToJsonList(allThings);

    } catch {
        throw new Error("No data found in Pod.")
    }
};

/**
 * Parse the nodes (= allThings) to a list of JSON objects. Each object contains "name", "category", "url" and "timestamp" of a webtracking record.
 * 1) Instantiate empty list.
 * 2) Loop over the nodes.
 * 3) Fetch the elements from the node.
 * 4) Add an object with the fetched elements to the list.
 * 5) Return the list.
 *
 * @param {*} allThings     list of nodes in the webtracking dataset
 * @returns JSON list of webtracking data.
 */
const turtleWebtrackingListToJsonList = (allThings) => {
    // 1)
    let webTrackingList = [];
    // 2)
    allThings.forEach((thing) => {
        /* BEGIN 3 */
        const name = getStringNoLocale(thing, `${SCHEMA_NAME_PROPERTY}`);
        const category = getStringNoLocale(
            thing,
            `${SCHEMA_CATEGORY_PROPERTY}`
        );
        const url = getUrl(thing, `${SCHEMA_URL_PROPERTY}`);
        const timestamp = getDatetime(thing, `${SCHEMA_DATE_PROPERTY}`);
        /* END 3 */

        // 4)
        webTrackingList.push({
            name: name,
            category: category,
            url: url,
            timestamp: timestamp,
        });
    });
    // 5)
    return webTrackingList;
};

/********************************/
/*  END - READING DATA FROM POD */
/********************************/

/*********************************/
/*  START - WRITING DATA TO POD  */
/*********************************/
/**
 * Write data to the pod.
 * 1) Build the resource url.
 * 2) Try to update the existing dataset (hosted on a certain resource url).
 * 3) When the resource is not found, we create a new dataset (= first time initialization).
 *
 * @param {*} eventName     name of the tracked event
 * @param {*} category      category of the tracked event
 * @param {*} hostUrl       url of the host that was tracked
 */
const writeDataToPod = (eventName, category, hostUrl) => {
    // 1)
    const resourceUrl = buildResourceUrl();
    // 2)
    updatePod(resourceUrl, eventName, category, hostUrl)
        .then((ignore) => {})
        .catch((err) => {
            // 3)
            if (err.statusCode === 404) {
                createDatasetAndSaveInPod(
                    resourceUrl,
                    eventName,
                    category,
                    hostUrl
                );
            }
        });
};

/**
 * Update the webtracking data in the users' pod.
 * 1) Fetch the dataset (resolves in a solid-dataset, throws error (404) when dataset was not found at "resourceUrl").
 * 2) Update and save the dataset.
 *
 * @param {*} resourceUrl   resource url of the tracking data in the users' pod
 * @param {*} eventName     name of the tracked event
 * @param {*} category      category of the tracked event
 * @param {*} hostUrl       url of the host that was tracked
 * @returns the updated dataset that was saved in the pod (or throws a 404 error).
 */
const updatePod = async (resourceUrl, eventName, category, hostUrl) => {
    // 1)
    const dataset = await fetchSolidDataset(resourceUrl);
    // 2)
    return updateAndSaveDataset(
        dataset,
        resourceUrl,
        eventName,
        category,
        hostUrl
    );
};

/**
 * Update and save the dataset in the users' pod.
 * 1) Add a thing to the dataset.
 * 2) Save the updated dataset to the pod.
 *
 * @param {*} dataset       current dataset that needs to be updated
 * @param {*} resourceUrl   resource url of the tracking data in the users' pod
 * @param {*} eventName     name of the tracked event
 * @param {*} category      category of the tracked event
 * @param {*} hostUrl       url of the host that was tracked
 * @returns the updated dataset that was saved in the pod (or throws a 404 error).
 */
const updateAndSaveDataset = async (
    dataset,
    resourceUrl,
    eventName,
    category,
    hostUrl
) => {
    // 1)
    const updatedDataset = addThingToDataset(
        dataset,
        eventName,
        category,
        hostUrl
    );
    // 2)
    return saveDatasetInPod(updatedDataset, resourceUrl);
};

/**
 * Add a thing to the dataset. Creates an unnamed node with an "eventName", "category", "hostUrl" and "timestamp".
 * The dataset is manipulated in-memory (later it will be saved to the pod as a brand new dataset).
 * Solid uses Turtle formatting for writing data to the pod, so we create the Turtle file:
 * 1) Create the subject.
 * 2) Create the predicates.
 * 3) Create the objects.
 * 4) Assign the created triples to the dataset.
 *
 * @param {*} dataset       current dataset that needs to be updated
 * @param {*} eventName     name of the tracked event
 * @param {*} category      category of the tracked event
 * @param {*} hostUrl       url of the host that was tracked
 * @returns the updated dataset (with new eventName, category, hostUrl and timestamp)
 */
const addThingToDataset = (dataset, eventName, category, hostUrl) => {
    // createThing() -> creates an unnamed node (the subject)
    const thing = buildThing(createThing())
        // .addUrl(predicate, object)
        .addUrl(RDF.type, `${THING_TYPE_PROPERTY}`)
        // .addStringNoLocale(predicate, object)
        .addStringNoLocale(`${SCHEMA_NAME_PROPERTY}`, eventName)
        // .addStringNoLocale(predicate, object)
        .addStringNoLocale(`${SCHEMA_CATEGORY_PROPERTY}`, category)
        // .addUrl(predicate, object)
        .addUrl(`${SCHEMA_URL_PROPERTY}`, hostUrl)
        // .addDatetime(predicate, object)
        .addDatetime(`${SCHEMA_DATE_PROPERTY}`, new Date())
        .build();

    // 4)
    return setThing(dataset, thing);
};

/**
 * Save the updated dataset in the current users' pod.
 *
 * saveSolidDatasetAt("resourceUrl", "updated dataset", "config")
 *      - config -> pass the imported "fetch" from the "@inrupt/solid-client-authn-browser". This makes sure that the native
 *        "fetch" method gets manipulated to use the correct tokens for authenticated calls.
 *
 * @param {*} dataset       updated dataset
 * @param {*} resourceUrl   resource url of the tracking data in the users' pod
 * @returns the saved dataset in a Promise
 */
const saveDatasetInPod = async (dataset, resourceUrl) => {
    return saveSolidDatasetAt(resourceUrl, dataset, {
        fetch: fetch,
    });
};

/**
 * Create an empty dataset and save in the users' pod. Also creates an ACL (Access Control List) for the resource.
 * 1) Create empty dataset.
 * 2) Update and save the dataset.
 * 3) Create the ACL file (the current user gets full access).
 *
 * @param {*} resourceUrl   resource url of the tracking data in the users' pod
 * @param {*} eventName     name of the tracked event
 * @param {*} category      category of the tracked event
 * @param {*} hostUrl       url of the host that was tracked
 */
const createDatasetAndSaveInPod = async (
    resourceUrl,
    eventName,
    category,
    hostUrl
) => {
    // 1)
    const emptyDataset = createSolidDataset();
    // 2)
    await updateAndSaveDataset(
        emptyDataset,
        resourceUrl,
        eventName,
        category,
        hostUrl
    );
    // 3)
    await createAndSaveAclForDataset(resourceUrl);
};

/**
 * Creates an Access Control List (ACL) file for the resource at a certain url.
 * 1) Fetch the current dataset.
 * 2) Create in-memory ACL file for the fetched dataset.
 * 3) Fetch the current session.
 * 4) Update the ACL file by setting the access values for the current user.
 * 5) Save the updated ACL file to the pod.
 *
 * @param {*} resourceUrl   resource url of the tracking data in the users' pod
 * @returns the created ACL dataset in a Promise.
 */
const createAndSaveAclForDataset = async (resourceUrl) => {
    // 1)
    const dataset = await fetchSolidDataset(resourceUrl);
    // 2)
    const acl = createAcl(dataset);
    // 3)
    const session = getDefaultSession();
    // 4)
    const updatedAcl = setAgentResourceAccess(acl, session?.info?.webId, {
        read: true,
        append: true,
        write: true,
        control: true,
    });
    // 5)
    return saveAclFor(dataset, updatedAcl, { fetch: fetch });
};
/*******************************/
/*  END - WRITING DATA TO POD  */
/*******************************/

/***********************************/
/*  START - OTHER HELPER FUNCTIONS */
/***********************************/
const extractData = (data) => {
    const eventName = data.event;
    const eventInfo = data.pageInfo;
    const category = eventInfo?.category;
    const hostUrl = eventInfo?.url;

    if (!eventName || !eventInfo || !category || !hostUrl) {
        throw new Error("Wrong data format!");
    }

    return {
        eventName: eventName,
        category: category,
        hostUrl: hostUrl,
    };
};

const buildResourceUrl = () => {
    const session = getDefaultSession();
    const webId = session?.info?.webId;
    const baseUrl = webId.split("/profile")[0];

    return `${baseUrl}/webhistory/org/data`;
};

/*********************************/
/*  END - OTHER HELPER FUNCTIONS */
/*********************************/

export default solidService;
